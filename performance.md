# Đo Performance và Debug Memory Leaks
## Đo Performance
### 1. Sử dụng XCTMetric Protocol

Trong XCode 11, Apple đã xây dựng các công cụ để kiểm tra hiệu suất dựa trên một giao thức mới - XCTMetric Protocol.

Các phương thức:
- XCTClockMetric - để đo thời gian chạy mã của bạn.
- XCTCPUMetric - để đo số chu kỳ CPU, số lệnh đã gỡ bỏ và thời gian CPU mà mã của bạn đã sử dụng.
- XCTStorageMetric - để đo lường việc sử dụng bộ nhớ cho mã của bạn.
- XCTMemoryMetric - để đo bộ nhớ vật lý mà mã của bạn đã sử dụng.

Cách sử dụng:

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        let test = DiscoverViewController()
        measure(metrics: [XCTClockMetric(), // to measure time
                          XCTCPUMetric(), // to measure CPU cycles
                          XCTStorageMetric(), // to measure storage consuming
                          XCTMemoryMetric()]) { // to measeure RAM consuming
            
            test.getTest(str: "Test") // this is the heavy process
        }
Sau khi chạy ta có thể xem hiệu suất:

<img src="https://i.stack.imgur.com/rFzty.png">

### 2. Sử dụng Debug Navigator
Xcode có bộ "Debug Navigator" bao gồm nhiều thông tin về hiệu suất của ứng dụng. Khi chạy ứng dung ta có thể vào đây để xem các thông số về CPU, Memory, Disk và Network.

![](https://developer.apple.com/library/archive/documentation/DeveloperTools/Conceptual/debugging_with_xcode/Art/dwx-dt-dbgnav-1_2x.png)

- Xem thông số CPU

![](https://i.stack.imgur.com/veILQ.png)
Có thể xem chi tiết bằng cách chọn vào "Profile Instruments" sẽ hiển thị giao diện
 
 ![](https://koenig-media.raywenderlich.com/uploads/2020/10/4_profiler-650x405.png)

Chọn Time Profile để theo dõi hiệu suất của CPU trong quá trình chạy ứng dụng.

 ![](https://koenig-media.raywenderlich.com/uploads/2020/10/5_profiler.png)

 Ở đây sẽ hiển thị:

    1. Điều khiển ghi : Nút ghi dừng và khởi động ứng dụng hiện đang được thử nghiệm. Nút tạm dừng tạm dừng quá trình thực thi hiện tại của ứng dụng.
    2. Bộ đếm thời gian chạy : Bộ đếm thời gian đếm thời gian ứng dụng được cấu hình đã chạy và số lần nó đã chạy. 
    3. Theo dõi công cụ : Đây là theo dõi Hồ sơ thời gian. 
    4. Bảng chi tiết : Bảng này hiển thị thông tin chính về công cụ cụ thể mà bạn đang sử dụng. Trong trường hợp này, nó hiển thị những cái sử dụng nhiều thời gian CPU nhất.
    5. Bảng trình kiểm tra.

- Xem thông số Memory

![](https://i.stack.imgur.com/Lw7T5.png)

- Xem thông số Disk

![](https://i.stack.imgur.com/NqK8g.png)

- Xem thông số Network

![](https://i.stack.imgur.com/S5jOU.png)

## Debug Memory Leak

### Memory Leaks là gì?
Memory leaks( Rò rỉ bộ nhớ) là hiện tượng bộ nhớ đã được cấp phát tại một số thời điểm, nhưng không bao giờ được giải phóng và không còn được ứng dụng của bạn tham chiếu. Nó tồn tại và gây tốn bộ nhớ và ảnh hưởng đến quá trình chạy ứng dụng và có thể gây crash.

### Sự nguy hiểm của việc rò rỉ bộ nhớ
Không chỉ làm tăng dung lương bộ nhớ của ứng dụng, chúng còn gây nên các hiệu ứng phụ và crashes. 

Khi bị leak, các objects không được giải phóng. Nhưng objects này đều là rác. Khi các thao tác khởi tạo ra nhưng đối tượng này được lặp lại, bộ nhớ chiếm đóng sẽ tăng lên. Quá nhiều rác, điều này sẽ dẫn đến memory warnings, và cuối cùng ứng dụng sẽ bị crashes.
### Vì sao xảy ra hiện tượng rò rỉ bộ nhớ
Leak có thể đến từ SDK hoặc framework của bên thứ 3 chẳng hạn. Hoặc có thể đến từ chính Apple như CALayer hay UILabel. Trong những trường hợp đó, chúng ta chẳng thể làm được gì ngoài việc chờ bản cập nhật hoặc phải huỷ SDK đó đi. Nhưng nhiều khả năng leaks là trong chính source code, một trong nhưng lý do đó là retain cycle.

### Retain cycle
Trong Swift, khi một đối tượng có một liên kết mạnh mẽ với một đối tượng khác, nó sẽ giữ lại nó (retain).

Khi một object tham chiếu đến một object thứ hai, nó sẽ sở hữu. Object thứ hai sẽ vẫn còn sống cho đến khi nó được giải phóng. Điều này được gọi là Strong reference. Chỉ khi bạn đặt thuộc tính là nil thì object thứ hai sẽ bị hủy.

Ví dụ:

    class A {
        var listB : [B] 
        func add(b:B){
            self.listB.append(b)
        }
    }
    class B {
        var a : A //And this one is also strong
        init (a : A) {
            self.a = a
            self.a.add(b:self)
        }
    }

Trong ví dụ này sẽ không thể giải phóng cả a và b.

Để được giải phóng khỏi bộ nhớ, một object trước hết phải giải phóng tất cả các phụ thuộc của nó. 

Vì chính object là một sự phụ thuộc, nó không thể được giải phóng. 

### Cách khắc phục 
Có 2 giải pháp cho vấn đề này đó là khiến 1 trong những thuộc tính trở thành weak hoặc unowned. 

Tham chiếu weak và unowned cho phép thực thể trong vòng lặp tham chiếu có thể chiếu tới đối tượng khác mà không giữ tham chiếu mạnh tới nó. 

Thực thể có thể tham chiếu tới đối tượng khác mà không tạo ra vòng lặp tham chiếu mạnh. So với liên kết mạnh, weak không làm tăng bộ đếm tham chiếu.

### Debug Memory Leaks
Sử dụng Xcode Memory Graph Debugger để xác định các "retain cycles" và "memory leaks".
1. Chọn các thuộc tính qua Xcode scheme editor.

![anh](https://developer.apple.com/library/archive/documentation/DeveloperTools/Conceptual/debugging_with_xcode/Art/malloc-stack-logging_2x.png)

- Chọn thuộc tính "Malloc Scribble" ở mục Memory Management
- Chọn "Malloc Stack" và chọn "Live Allocations Only" trong phần Logging

2. Chọn chế độ graph debug

    Chọn biểu đồ debug bộ nhớ bằng cách chọn biểu tượng thanh gỡ lỗi như hình.

    ![anh](https://useyourloaf.com/blog/xcode-visual-memory-debugger/001.png)

3. Các thông số trong trình debug

    Trình debug này sẽ tạm dừng các tiến trình xử lý của ứng dụng và hiển thị các mục như sau:

    ![anh](https://useyourloaf.com/blog/xcode-visual-memory-debugger/002@2x.png)

    Phía bên trái là các Heap (Heap Content): Nơi chứa các object được sử dụng.

    Ở giữa sẽ hiện thị các mối liên hệ các tham chiếu tới object được chọn từ vùng Heap: Được gọi là Object references.

    Sau khi chọn các references ở màn hình giữa thanh backtrace bên phải sẽ hiển thị các thông tin bộ nhớ của đối tượng.
    
    ![anh](https://doordash.engineering/wp-content/uploads/2019/05/Screen-Shot-2019-05-04-at-3.47.53-PM.png)

    Các dòng in đậm có nghĩa là có một tham chiếu mạnh mẽ đến đối tượng mà nó trỏ tới.

    Các đường màu xám nhạt có nghĩa là có một tham chiếu không xác định (có thể là yếu hoặc mạnh) đến đối tượng mà nó trỏ tới.

    Nhấn vào một phiên bản từ bảng điều khiển bên trái sẽ chỉ hiển thị cho bạn chuỗi tham chiếu đang giữ đối tượng đã chọn trong bộ nhớ. Nhưng nó sẽ không hiển thị cho bạn những tham chiếu nào mà đối tượng được chọn có tham chiếu đến.

    Nếu xảy ra hiện tượng Memory leaks thì phần thông báo sẽ hiển thị với biểu tượng ! màu tím như hình:

    ![anh](https://useyourloaf.com/blog/xcode-visual-memory-debugger/004.png)
  
    Chọn vào 1 leak để xem vấn đề gặp phải:

    
    ![](https://useyourloaf.com/blog/xcode-visual-memory-debugger/005.png)

Xem liên kết giữa các object vào tiến hành gỡ lỗi.

